# Maintainer: Mark Wagie <mark at manjaro dot org>
# Contributor: Fabio 'Lolix' Loli <lolix@disroot.org> -> https://github.com/FabioLolix
# Contributor: Helmut Stult

pkgname=popsicle
pkgver=1.3.3
pkgrel=2
pkgdesc="Linux utility for flashing multiple USB devices in parallel, written in Rust"
arch=('x86_64' 'aarch64')
url="https://github.com/pop-os/popsicle"
license=('MIT')
depends=(
  'gtk3'
)
makedepends=(
  'cargo'
  'clang'
  'git'
  'help2man'
  'xorgproto'
)
source=("git+https://github.com/pop-os/popsicle.git#tag=$pkgver")
sha256sums=('6ecc7d671bee4eb2cc0670e2e74ec2e45c392e3096b11cc2a326f085edf712a1')

prepare() {
  cd "$pkgname"

  # fix: update loopdev to loopdev-erikh
  # https://github.com/pop-os/popsicle/issues/208
  git cherry-pick -n 3382c77e874054821ac6b9bb22a44417fcfd3e9b

  export RUSTUP_TOOLCHAIN=stable
  cargo fetch --locked --target "$(rustc -vV | sed -n 's/host: //p')"
}

build() {
  cd "$pkgname"
  export RUSTUP_TOOLCHAIN=stable
  ARGS+=" --frozen" make
}

package() {
  cd "$pkgname"
  make DESTDIR="$pkgdir" prefix=/usr install

  install -Dm644 LICENSE -t "$pkgdir/usr/share/licenses/$pkgname/"
}
